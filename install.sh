git submodule update --init --recursive
curl -L https://bit.ly/janus-bootstrap | bash
python `pwd`/vim/_janus/YouCompleteMe/install.py
ln -sf `pwd`/vim/_janus "$HOME/.janus"
ln -sf `pwd`/vim/_vimrc.after "$HOME/.vimrc.after"
ln -sf `pwd`/vim/_vimrc.before "$HOME/.vimrc.before"
ln -sf `pwd`/tmux/_tmux.conf "$HOME/.tmux.conf"
git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"
ln -sf `pwd`/zsh/_zpreztorc "$HOME/.zpreztorc"
ln -sf `pwd`/zsh/_zshrc "$HOME/.zshrc"
cd "$HOME"
